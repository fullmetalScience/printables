echo(version=version());
echo("From command line use -D '$varname=value' to make your selection.");

preview_resolution = 80;
render_resolution = 100;
$fn = $preview ? 80 : render_resolution;


// Part helper
module part(object, as = "cube") {
    dimensions = [object.x, object.y, object.z];
    if (as == "cube")
        cube(dimensions);
    else if (as == "cylinder")
        cylinder(d = dimensions.x, h = dimensions.z);
    else if (as == "minkowski") {
        minkowski_d = 4;
        translate([minkowski_d/2, minkowski_d/2, 0]) minkowski() {
                // subtrahend can be anything between zero (exclusive) and height (exclusive)
                subtrahend = 1;
                cube( [dimensions.x-minkowski_d,
                       dimensions.y-minkowski_d,
                       dimensions.z-subtrahend] );
                cylinder(d=minkowski_d, h=subtrahend);
        }
    }
}

nozzle      = .4;
wall        = 1.6;
tolerance   = nozzle;
intowall    = nozzle;

// Model

// https://en.wikipedia.org/wiki/.22_Long_Rifle#/media/File:22_long_rifle.svg
cartridge   = [ 6.90, 6.90, 25.18 ];
rim         = [ 6.90, 6.90, 1.016 ];
shell       = [ 5.69, 5.69, 14.98-rim.z ];
// Activate this tolerance when you want to print the slide only
//shell       = [ 5.69+tolerance/2, 5.69+tolerance/2, 14.98-rim.z ];
tip         = [ 5.66, 5.66, cartridge.z-shell.z-rim.z ];



// slide measured from the holder that comes with CCI 50-cartridge ammo boxes
slide       = [ 37.30, 71.30,  3.15 ];
zpos_slide  = wall + cartridge.z - slide.z - rim.z;
box         = [ wall-intowall + slide.x + wall-intowall,
                slide.y + 2*wall - .1,
                wall + tolerance + cartridge.z + tolerance + wall + .82 ];
slot        = [ box.x, 1.6*rim.x+wall, box.z ];
railgap     = [ intowall + slide.x + intowall,
                box.y + slot.y -2*wall,
                slide.z + rim.z + tolerance+.1 ]; // required space only
                // box.z - wall - zpos_slide ]; // entire remaining height up to ceiling
frontgap    = [ railgap.x, railgap.y+wall+1, slide.z + 2*tolerance];
boltgap     = [ box.x+1, 6*nozzle, 6*nozzle ];
upperslide  = [ box.x-2*wall+2*intowall, 16*nozzle, wall ];
thumbgap    = [ 25, 2*wall, box.z-2*wall ];
windowgap   = [ 999, box.y/3.2-3*wall, 10 ];
inner_space = [ box.x - 2*wall - nozzle, box.y+slot.y-2*wall, box.z - 2*wall ];
slotgap     = [ box.x-2*wall+2*intowall, slot.y, 999 ];
labelgap    = [ 10, 68, nozzle+1 ];

debug_removetop = [box.x-2*wall, box.y, wall+2]; 

echo(box=box);


module cartridge (form = "cylinder") {
        union() {
                part(rim, "cylinder");
                translate([0,0,rim.z]) {
                        part(shell, form);
                        color("DimGray") translate([0,0,shell.z]) cylinder(tip.z,tip.x/2,tip.y/2.8);
                }
        }
}

module 50_cartridges() {
        outer_clearance=3*nozzle;
        remaining_slide=[slide.x-2*outer_clearance, slide.y-2*outer_clearance, 0];
        available_clearances=[remaining_slide.x  -5*shell.x - outer_clearance,
                              remaining_slide.y -10*shell.y - outer_clearance, 0];
        one_xclearance=available_clearances.x/5;
        one_yclearance=available_clearances.y/10;
        echo(one_xclearance=one_xclearance);
        echo(one_xclearance + tolerance/2  >  1);
        echo(one_yclearance=one_yclearance);
        translate([outer_clearance, outer_clearance, 0]) {
                for ( xpos=[0:4] ) {
                        for ( ypos=[0:9] ) {
                                mirror([0,0,1]) {
                                        translate([ (1+xpos) *  remaining_slide.x/5  -remaining_slide.x/5 + one_xclearance/2,
                                                    (1+ypos) * remaining_slide.y/10 -remaining_slide.y/10 + one_yclearance/2,
                                                    -slide.z-rim.z - tolerance/2 ])

                                        cartridge("cube");
                                }
                        }
                }
        }
}



module ramp() {
        rampblock = [ box.x, 18, 10];
        intersection() {
                translate([0, box.y+slot.y-rampblock.y/2-2,-7.7]) rotate([20,0,0])
                        part(rampblock);
                translate([box.x/2-inner_space.x/2, wall, wall]) part(inner_space);
        }
} color("MediumSeaGreen") ramp();

module dot_id() {
        rotate([90,0,90]) scale([1,1,1]) linear_extrude(nozzle)
                import("xmrid_dot_id.svg");
}

module box() {
        difference() {
                union() {
                        part(box);
                        translate([0,box.y]) part(slot);
                        translate([box.x, box.y-2*wall+nozzle, 2*wall]) dot_id();
                        translate([0-nozzle, box.y-2*wall+nozzle, 2*wall]) dot_id();
                }
                translate([wall+5*nozzle, wall+inner_space.y+nozzle, 24.5*nozzle])
                        rotate([90,0,0]) linear_extrude(nozzle) scale([.7,.7,.7])
                                import("xmrid_with_sparks.svg");

                translate([box.x/2-slotgap.x/2, box.y-wall, zpos_slide]) part(slotgap);
                translate([0, box.y-wall, box.z-26*nozzle]) part(boltgap);
                translate([wall-intowall, box.y-wall, box.z-wall])
                        rotate([206,0,0]) part(upperslide);
                *translate([wall,wall,box.z-wall-1]) part(debug_removetop);

                ypos_label=(box.y-wall)/2 - labelgap.y/2;
                translate([6*nozzle, ypos_label, box.z-nozzle]) part(labelgap);
                translate([box.x/2, box.y/2-wall, box.z-nozzle]) rotate([0,0,90])
                        linear_extrude(nozzle+1) text(".22 LR", font = "Eurofighter",
                             halign = "center", valign = "center");
                translate([box.x-labelgap.x-6*nozzle, ypos_label, box.z-nozzle]) part(labelgap);

                translate([box.x/2-inner_space.x/2, wall, wall]) part(inner_space);
                translate([wall-2*intowall, wall, zpos_slide]) part(railgap);
                translate([wall-2*intowall, wall, zpos_slide]) part(frontgap);
                translate([(box.x-thumbgap.x)/2, -1, box.z-wall-thumbgap.z]) part(thumbgap);

                translate([-1, 3*wall, 2*wall]) {
                        part(windowgap);
                        translate([-1, windowgap.y + 3*wall, 0]) {
                                part(windowgap);
                                        translate([-1, windowgap.y + 3*wall, 0]) part(windowgap);
                        }
                }
        }
} color("Honeydew") box();


module round_count (label) {
        fontsize=2.8;
        translate([0,0, box.z-3.3*wall]) {
                translate([0, 1.5*wall])
                        rotate([90,0,90]) linear_extrude(nozzle)
                        text(label, size=fontsize, font = "Eurofighter", halign = "left");
                for ( amount = [1:10] ) {
                        translate([0, amount*(rim.y+.1) - 1.2, -4.8*wall])
                        {
                                rotate([90,0,90]) linear_extrude(nozzle)
                                        text(str(5*amount), size=fontsize, font = "Eurofighter", halign = "center", valign = "center");
                                translate([0,0,-fontsize-nozzle]) cube([nozzle,nozzle,wall]);
                        }
                }
        }
}
translate([box.x,0,0]) round_count("stored");
translate([0,box.y+slot.y+.5,0]) rotate([0,0,180]) round_count("remaining");

module slide_with_cartridges() {
        translate([box.x/2-slide.x/2, wall+(box.y*$t), zpos_slide]) {
                difference() {
                        color("CadetBlue") part(slide, "minkowski");
                        50_cartridges();
                }
                // disable this if you want to print the slide only
                // TODO make this a condition
                //50_cartridges();
        }
} #slide_with_cartridges();



// Viewport
/*
viewport_distance = 300;
$vpd = viewport_distance;

down_from_top=80; lean_to_the_right=0; go_around=145;
viewport_rotation = [down_from_top, lean_to_the_right, go_around+360*$t];
$vpr = viewport_rotation;

viewport_translation = [box.x/2, box.y/2, box.z];
$vpt = viewport_translation;
*/
