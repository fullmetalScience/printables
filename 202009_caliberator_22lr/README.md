**Building the STL**

# Install [Eurofighter font](https://www.dafont.com/eurofighter.font).
# Generate the STL `openscad caliberator_22lr.scad -o caliberator_22lr.stl`.
# If you need the slide, adapt the scad file accordingly and repeat the above step.
