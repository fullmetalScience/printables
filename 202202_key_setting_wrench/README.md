# Key-Setting Wrench "Umstellaktivator" for LaGard 3330/3390

This is a model of a tool required to change the combination on LaGard 3330/3390 locks typically used in safes.

Use this model as reference for crafting your own tool from a steel rod or allen wrench.

**IMPORTANT:** *The torque required when using the tool exceeds the tensile strength of most 3D-printed filaments.
Abstain from using a printed version in an actual lock to avoid the risk of the lock becoming unusable.*

An **STL** can be build using OpenSCAD: `openscad key_setting_wrench.scad -o key_setting_wrench.stl`
