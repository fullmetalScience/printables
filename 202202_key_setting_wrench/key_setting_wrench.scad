echo(version=version());
echo("From command line use -D '$varname=value' to make your selection.");

preview_resolution = 80;
render_resolution = 100;
$fn = $preview ? 80 : render_resolution;

// Model

body = [ 4.73, 4.73, 83.1 ];
body_intersect_dimension = 3.35;

tip_sphere = 2.16;
tip_cylinder = [ tip_sphere, tip_sphere, 1 ];
taper = [ tip_cylinder.x, body.x, 1.3 ];

cutout_to_begin_of_taper_height = 22.5;
cutout = [ 3.42, 5 , 10.4 ];

handle = [ body.x, body.y, 25 ];


module tip() {
        sphere(d=tip_sphere);
        color("red") cylinder(d=tip_sphere, h=tip_cylinder.z);
}
module taper() {
        color("blue") cylinder(d1=taper.x, d2=taper.y, h=taper.z);
}
module body() {
        color("grey") cylinder(d=body.x, h=body.z);
}
module cutintersect() {
        color("white") cube([body.x,body.y,95]);
}
module cutout() {
        cube(cutout);
}

module handle() {
        scale([1, 1 ,1]) cylinder(d=handle.x, h=handle.z);
        //translate([-body.x/2, 0 , 0]) cube(handle);
        translate([-2, 1.8 , 0]) rotate([23,0,0]) cube([4, 4, 11]);
}
module handlecut() {
        difference () {
                handle();
                translate([-6/2,-5.1,0]) cube([6,4,handle.z+.01]);
        }
}

module uncut() {
        tip();
        translate ([0,0,tip_cylinder.z]) {
                taper();
        
                translate ([0,0,taper.z]) {
                        difference () {
                                body();
                                translate([-body.x/2+cutout.x,-cutout.y/2,cutout_to_begin_of_taper_height]) cutout();
                        }
                        translate ([0,0,body.z]) {
                                translate([0, -body.x/2 , 0]) rotate([-90,0,0]) handlecut();
                        }
                }
        }
}

module cut() {
        difference () {
                uncut();
                translate([-body.x/2,-body.y/2-body_intersect_dimension,-tip_sphere/2]) cutintersect();
        }
} rotate([90,0,0]) cut();
